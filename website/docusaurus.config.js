module.exports = {
  title: 'NexScript',
  tagline: 'Smart contracts for Nexa',
  url: 'https://nexscript.org',
  baseUrl: '/',
  favicon: 'img/favicon.ico',
  organizationName: 'NexScript',
  projectName: 'nexscript',
  onBrokenLinks: 'warn',
  themeConfig: {
    prism: {
      theme: require('prism-react-renderer/themes/nightOwlLight'),
      darkTheme: require('prism-react-renderer/themes/nightOwl'),
      additionalLanguages: ['solidity', 'antlr4'],
    },
    algolia: {
      // The application ID provided by Algolia
      appId: 'YMK16CA9DW',
      // Public API key: it is safe to commit it
      apiKey: '99e7315d4ffb00187cd912a33a493226',
      indexName: 'nexscript',
      // Optional: see doc section below
      contextualSearch: false,
      placeholder: "Search doc...",
      // Optional: Algolia search parameters
      searchParameters: {},
      // Optional: path for search page that enabled by default (`false` to disable it)
      searchPagePath: false,
      // Optional: whether the insights feature is enabled or not on Docsearch (`false` by default)
      insights: false,
      //... other Algolia params
    },
    image: 'img/nexscript-logo.svg',
    navbar: {
      logo: {
        alt: 'NexScript',
        src: 'img/nexscript-logo.svg',
      },
      items: [
        {to: '/docs/basics/about', label: 'Docs', position: 'right'},
        {
          href: 'https://playground.nexscript.org',
          label: 'Playground',
          position: 'right',
        },
        {
          href: 'https://gitlab.com/nexa/otoplo/nexscript',
          label: 'GitLab',
          position: 'right',
        },
      ],
      style: 'dark',
    },
    footer: {
      style: 'light',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Getting Started',
              to: '/docs/basics/getting-started',
            },
            {
              label: 'Examples',
              to: '/docs/language/examples',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Telegram',
              href: 'https://t.me/nexscript',
            },
          ],
        },
        {
          title: 'Built by',
          items: [
            {
              html: `
                <a href="https://otoplo.com" target="_blank">
                  <img src="/img/otoplo.svg" alt="Otoplo"
                       style="border-radius: 5px; max-height: 40px" />
                </a>
              `,
            },
          ],
        },
        {
          title: 'Funded by',
          items: [
            {
              html: `
                <a href="https://www.bitcoinunlimited.info/" target="_blank">
                  <img src="/img/BU.svg" alt="Bitcoin Unlimited"
                       style="border-radius: 5px; max-height: 30px" />
                </a>
              `,
            },
          ],
        },
      ],
      copyright: `This project is a fork of the original 
        <a href="https://cashscript.org" target="_blank">CashScript</a> project.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/otoplo/nexscript/edit/master/website/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
  plugins: [
    [
      '@docusaurus/plugin-client-redirects',
      {
        fromExtensions: ['html'],
        redirects: [
          { from: ['/docs', '/docs/about', '/docs/basics'], to: '/docs/basics/about'},
          { from: '/docs/language', to: '/docs/language/contracts' },
          { from: '/docs/sdk', to: '/docs/sdk/instantiation' },
          { from: '/docs/guides', to: '/docs/guides/covenants' },
          { from: '/docs/getting-started', to: '/docs/basics/getting-started' },
          { from: '/docs/examples', to: '/docs/language/examples' },
        ],
      },
    ],
  ],
};
