# @nexscript/utils

[![Build Status](https://travis-ci.org/otoplo/nexscript.svg)](https://travis-ci.org/otoplo/nexscript)
[![Coverage Status](https://img.shields.io/codecov/c/github/otoplo/nexscript.svg)](https://codecov.io/gh/otoplo/nexscript/)
[![NPM Version](https://img.shields.io/npm/v/@nexscript/nexscript.svg)](https://www.npmjs.com/package/@nexscript/nexscript)
[![NPM Monthly Downloads](https://img.shields.io/npm/dm/@nexscript/nexscript.svg)](https://www.npmjs.com/package/@nexscript/nexscript)
[![NPM License](https://img.shields.io/npm/l/@nexscript/nexscript.svg)](https://www.npmjs.com/package/@nexscript/nexscript)

`@nexscript/utils` is a package containing a number of utilities and types used by both the NexScript compiler and the JavaScript SDK. This package can be used by other applications as well, but it should be considered an unstable API and will not necessarily follow semantic versioning rules. So if you want to use `@nexscript/utils` as a standalone package, it is recommended to specify a specific version (instead of a range) in your `package.json`.
