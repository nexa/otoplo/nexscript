import { SourceArtifact } from '@nexscript/utils';
import {
  Contract,
  ElectrumNetworkProvider,
} from '../../src/index.js';
import { fund } from '../fixture/vars.js';

describe('Introspection contract', () => {
  beforeAll(async () => {
  });

  it('should test token introspection operations', async () => {
    // given
    const artifact = {
      contracts: [{
        contracts: [],
        contractName: 'test',
        constructorInputs: [],
        abi: [{
          name: 'test',
          inputs: [
          ],
        }],
        bytecode: 'OP_0 OP_UTXOBYTECODE OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_NIP OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_DROP OP_BIN2NUM OP_ENDIF OP_ENDIF OP_0 OP_NUMEQUALVERIFY OP_0 OP_UTXOBYTECODE OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_DROP 20 OP_SPLIT OP_DROP OP_ENDIF OP_0 OP_EQUALVERIFY OP_0 OP_UTXOBYTECODE OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_DROP OP_SIZE 20 OP_EQUAL OP_IF OP_DROP OP_0 OP_ENDIF OP_ENDIF OP_0 OP_EQUALVERIFY OP_0 OP_UTXOBYTECODE OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_2DROP OP_0 OP_ELSE OP_SPLIT OP_DROP 20 OP_SPLIT OP_NIP OP_ENDIF OP_0 OP_EQUALVERIFY OP_0 OP_UTXOBYTECODE OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_DROP OP_ELSE OP_SPLIT OP_NIP OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_NOTIF OP_DUP 51 60 OP_WITHIN OP_NOTIF OP_SPLIT OP_NIP OP_DUP OP_ENDIF OP_ENDIF OP_DROP OP_ENDIF OP_1 OP_SPLIT OP_SWAP OP_DUP 51 60 OP_WITHIN OP_IF OP_DROP OP_ELSE OP_SPLIT OP_NIP OP_ENDIF OP_1 OP_SPLIT OP_SWAP OP_DUP 00 OP_EQUAL OP_IF OP_DROP OP_ELSE OP_SPLIT OP_NIP OP_ENDIF OP_0 OP_EQUALVERIFY',
      }],
      source: `contract test() {
        function test() {
            require(tx.inputs[0].tokenAmount == 0);
            require(tx.inputs[0].tokenGroupId == 0x);
            require(tx.inputs[0].tokenSubgroupId == 0x);
            require(tx.inputs[0].subgroupData == 0x);
            require(tx.inputs[0].visibleParameters == 0x);
        }
      }`,
      compiler: { name: 'nexc', version: '0.1.0' },
      updatedAt: '2023-08-06T13:39:43.119Z',
    } as SourceArtifact;

    const provider = new ElectrumNetworkProvider();
    const contract = new Contract(artifact, [], { provider });
    await fund(contract.address, 100000);

    // when
    const txPromise = contract.functions
      .test()
      .to(contract.address, 90000n)
      .send();

    // then
    await expect(txPromise).resolves.not.toThrow();
  });
});
