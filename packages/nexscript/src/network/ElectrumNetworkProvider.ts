import {
  ElectrumCluster,
  ElectrumTransport,
  ClusterOrder,
  RequestResponse,
} from '@vgrunner/electrum-cash';
import { Utxo, Network } from '../interfaces.js';
import NetworkProvider from './NetworkProvider.js';

export default class ElectrumNetworkProvider implements NetworkProvider {
  private electrum: ElectrumCluster;
  private concurrentRequests: number = 0;

  constructor(
    public network: Network = globalThis.process?.env?.JEST_WORKER_ID === undefined ? Network.MAINNET : Network.REGTEST,
    electrum?: ElectrumCluster,
    private manualConnectionManagement?: boolean,
  ) {
    // If a custom Electrum Cluster is passed, we use it instead of the default.
    if (electrum) {
      this.electrum = electrum;
      return;
    }

    if (network === Network.MAINNET) {
      this.electrum = new ElectrumCluster('NexScript Application', '1.4.1', 1, 1, ClusterOrder.PRIORITY, undefined, undefined, true);
      this.electrum.addServer('electrum.nexa.org', 20004, ElectrumTransport.WSS.Scheme, false);
    } else if (network === Network.TESTNET) {
      this.electrum = new ElectrumCluster('NexScript Application', '1.4.1', 1, 1, ClusterOrder.PRIORITY, undefined, undefined, true);
      this.electrum.addServer('testnet-explorer.nexa.org', 30004, ElectrumTransport.WSS.Scheme, false);
    } else if (network === Network.REGTEST) {
      this.electrum = new ElectrumCluster('NexScript Application', '1.4.1', 1, 1, ClusterOrder.PRIORITY, undefined, undefined, true);
      this.electrum.addServer('127.0.0.1', 30403, ElectrumTransport.WS.Scheme, false);
    } else {
      throw new Error(`Tried to instantiate an ElectrumNetworkProvider for unsupported network ${network}`);
    }
  }

  async getUtxos(address: string): Promise<Utxo[]> {
    const [allUtxos, tokenUtxos] = await Promise.all([
      this.performRequest('blockchain.address.listunspent', address) as unknown as ElectrumUtxo[],
      this.performRequest('token.address.listunspent', address) as unknown as RostrumTokenResponse,
    ]);

    const completeUtxos: Array<ElectrumUtxo & RostrumTokenUtxo> = allUtxos.map((val) => {
      const tokenUtxo = tokenUtxos.unspent.find((utxo: RostrumTokenUtxo) => utxo.tx_hash === val.tx_hash
        && utxo.tx_pos === val.tx_pos)!;
      return { ...val, ...tokenUtxo };
    });

    const utxos = completeUtxos.map((utxo) => ({
      // txid: utxo.tx_hash,
      txid: (utxo as any).outpoint_hash,
      vout: utxo.tx_pos,
      satoshis: BigInt(utxo.value),
      address,
      token: utxo.group ? {
        groupId: utxo.token_id_hex,
        amount: BigInt(utxo.token_amount),
      } : undefined,
    } as Utxo));

    return utxos;
  }

  async getBlockHeight(): Promise<number> {
    const { height } = await this.performRequest('blockchain.headers.subscribe') as BlockHeader;
    return height;
  }

  async getRawTransaction(txid: string): Promise<string> {
    return await this.performRequest('blockchain.transaction.get', txid) as string;
  }

  async sendRawTransaction(txHex: string): Promise<string> {
    return await this.performRequest('blockchain.transaction.broadcast', txHex) as string;
  }

  async connectCluster(): Promise<void[]> {
    try {
      return await this.electrum.startup();
    } catch (e) {
      return [];
    }
  }

  async disconnectCluster(): Promise<boolean[]> {
    return this.electrum.shutdown();
  }

  async performRequest(
    name: string,
    ...parameters: (string | number | boolean)[]
  ): Promise<RequestResponse> {
    // Only connect the cluster when no concurrent requests are running
    if (this.shouldConnect()) {
      this.connectCluster();
    }

    this.concurrentRequests += 1;

    await this.electrum.ready();

    let result;
    try {
      result = await this.electrum.request(name, ...parameters);
    } finally {
      // Always disconnect the cluster, also if the request fails
      // as long as no other concurrent requests are running
      if (this.shouldDisconnect()) {
        await this.disconnectCluster();
      }
    }

    this.concurrentRequests -= 1;

    if (result instanceof Error) throw result;

    return result;
  }

  private shouldConnect(): boolean {
    if (this.manualConnectionManagement) return false;
    if (this.concurrentRequests !== 0) return false;
    return true;
  }

  private shouldDisconnect(): boolean {
    if (this.manualConnectionManagement) return false;
    if (this.concurrentRequests !== 1) return false;
    return true;
  }
}

interface ElectrumUtxo {
  tx_pos: number;
  value: number;
  tx_hash: string;
  height: number;
  has_token: boolean;
}

interface RostrumTokenUtxo {
  group: string;
  height: number;
  outpoint_hash: string;
  token_amount: number;
  token_id_hex: string;
  tx_hash: string;
  tx_pos: number;
  value: number;
}

interface RostrumTokenResponse {
  cursor: null;
  unspent: RostrumTokenUtxo[];
}

interface BlockHeader {
  height: number;
  hex: string;
}
