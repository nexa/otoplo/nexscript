import {
  ContractAccessorNode,
  HexLiteralNode,
} from '../ast/AST.js';
import AstTraversal from '../ast/AstTraversal.js';

export default class ContractAccessorTraversal extends AstTraversal {
  constructor(public replacements: any) {
    super();
  }

  visitContractAccessor(node: ContractAccessorNode): ContractAccessorNode {
    const key = `${node.contract.name}${node.accessor}`;
    if (this.replacements[key]) {
      return new HexLiteralNode(this.replacements[key]) as any;
    }

    return node;
  }
}
