import { CircularDependencyError } from '../Errors.js';
import {
  ContractNode,
  ContractAccessorNode,
} from '../ast/AST.js';
import AstTraversal from '../ast/AstTraversal.js';

type DependencyMap = { [contractName: string]: string[] };

export default class DependencyTraversal extends AstTraversal {
  public dependencyMap: DependencyMap = {};
  private currentContract: string;

  visitContract(node: ContractNode): ContractNode {
    this.currentContract = node.name;
    this.dependencyMap[this.currentContract] = [];

    node.contracts.forEach((contract) => this.visitContract(contract) as ContractNode);

    return super.visitContract(node) as ContractNode;
  }

  visitContractAccessor(node: ContractAccessorNode): ContractAccessorNode {
    if (this.dependencyMap[this.currentContract].indexOf(node.contract.name) === -1) {
      this.dependencyMap[this.currentContract].push(node.contract.name);
    }

    return super.visitContractAccessor(node) as ContractAccessorNode;
  }

  buildDependencyList(contract: string, dependencies: DependencyMap): string[] {
    const nodes: any = {};
    let nodeCount = 0;
    const ready: any[] = [];
    const output = [];

    const add = (element: string): void => {
      nodeCount += 1;
      nodes[element] = { needs: [], neededBy: [], name: element };
      if (dependencies[element]) {
        dependencies[element].forEach((dependency) => {
          if (!nodes[dependency]) {
            add(dependency);
          }
          nodes[element].needs.push(nodes[dependency]);
          nodes[dependency].neededBy.push(nodes[element]);
        });
      }
      if (!nodes[element].needs.length) {
        ready.push(nodes[element]);
      }
    };

    if (contract) {
      add(contract);
    } else {
      for (const element in dependencies) {
        if (!nodes[element]) {
          add(element);
        }
      }
    }

    // sort
    while (ready.length) {
      const dependency = ready.pop();
      output.push(dependency.name);
      dependency.neededBy.forEach((element: any) => {
        element.needs = element.needs.filter((x: any) => x !== dependency);
        if (!element.needs.length) {
          ready.push(element);
        }
      });
    }

    if (output.length !== nodeCount) {
      throw new CircularDependencyError(contract);
    }

    return output.slice(0, -1);
  }
}
