import fs from 'fs';
import { URL } from 'url';
import { stringify } from '@bitauth/libauth';
import { parseCode, compileString } from '../../src/compiler.js';

describe('Location', () => {
  it('should retrieve correct text from location', () => {
    const code = fs.readFileSync(new URL('../valid-contract-files/simple_functions.nex', import.meta.url), { encoding: 'utf-8' });
    const ast = parseCode(code);

    const f = ast.contracts[0].functions[0];

    expect(f.location).toBeDefined();
    expect((f.location!).text(code)).toEqual('function hello(sig s, pubkey pk) {\n        require(checkSig(s, pk));\n    }');
  });

  it('should fail to compile a script with no contracts', () => {
    const code = '';

    expect(() => parseCode(code)).toThrow('Source file should contain at leas one contract');
  });

  it('should fail to compile two contracts with same name', () => {
    const code = `
    contract A() {
      function A(string vA, int x) {
          require(vA == "A");
          require(x == 1);
      }
    }

    contract A() {
      function A(string vA, int x) {
          require(vA == "A");
          require(x == 1);
      }
    }
  `;

    expect(() => parseCode(code)).toThrow('All contracts should have unique names');
  });

  it('should compile multi contract script', () => {
    const code = `
    contract A() {
      function A(string vA, int x) {
          require(vA == "A");
          require(x == 1);
      }

      function B(string vA, int x) {
        require(vA == "B");
        require(x == 2);
      }
    }

    contract B() {
      function Bv(string vB) {
          require(vB == "B");
      }
    }
    `;

    expect(() => parseCode(code)).not.toThrow();
  });

  it('should inline locking bytecode', () => {
    const code = `contract A() {
      function A(string vA, int x) {
        bytes lockingBytecode = B.lockingBytecode;
        require(lockingBytecode.length != 0);

        require(vA == "A");
        require(x == 1);
      }

      function B(string vA, int x) {
        require(vA == "B");
        require(x == 2);
      }
    }

    contract B() {
      function Bv(string vB) {
        require(vB == "B");
      }
    }

    contract C() {
      function Bv(string vB) {
        bytes20 hash = A.templateHash;
        require(hash.length != 0);
        require(vB == "B");
      }
    }
    `;

    expect(() => parseCode(code)).not.toThrow();
    console.log(compileString(code));
  });

  it('should test compilation failures of nested contracts', async () => {
    expect(() => parseCode(`
      contract A() {
        contract B() {
          contract C() {
            function test() {
              require(1 == 1);
            }
          }
        }
      }
    `)).toThrow("Contracts are allowed to be nested by only one level");

    expect(() => parseCode(`
      contract MCP() {
        contract MAST() {
          function test() {
            require(1 == 1);
          }
        }

        function mcpFunction() {
          require(2 == 2);
        }
      }
    `)).toThrow("Contracts with nested contracts (MCP) can not have functions");
  });

  it('should allow nested contracts', async () => {
    const code = `
    contract A() {
      function a() {
        require(1 == 1);
      }
    }

    contract B() {
      function a() {
        require(2 == 2);
      }
    }

    contract MCP() {
      contract MastA() {
        function Bv(string vB) {
          bytes20 hash2 = B.templateHash;
          require(hash2.length != 0);
          require(vB == "B");
        }
      }

      contract MastB() {
        function Bv(string vB) {
          bytes20 hash = A.templateHash;
          require(hash.length != 0);
          bytes20 hash2 = B.templateHash;
          require(hash2.length != 0);
          bytes20 hash3 = MastA.templateHash;
          require(hash3.length != 0);
          require(vB == "B");
        }
      }
    }
    `;

    expect(() => compileString(code)).not.toThrow();
  });
});
